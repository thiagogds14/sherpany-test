function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});


function initMap() {
    var rio = {lat: -22.907391, lng: -43.175665};
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 14,
      center: rio
    });
    var layer = new google.maps.FusionTablesLayer({
        query: {
            select: '\'Location\'',
            from: '1VAuRAnKvb2CROkxnkKPonThuZSk2s6fHPZ2XnzRl'
        }
    });
    layer.setMap(map);

    var geocoder = new google.maps.Geocoder;
    var addresses_ul = $("#addresses ul")
    map.addListener('click', function(e) {
        geocoder.geocode({'location': e.latLng}, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                aprox_location = results[0]
                allowed_result_type = 'street_address'
                if (aprox_location.geometry.location_type == google.maps.GeocoderLocationType.ROOFTOP ||
                    aprox_location.geometry.location_type == google.maps.GeocoderLocationType.RANGE_INTERPOLATED &&
                    aprox_location.types[0] == 'street_address') {
                        var result = results[0]
                        var data = {
                            'lat': result.geometry.location.lat(),
                            'lng': result.geometry.location.lng(),
                            'formatted_address': result.formatted_address
                        }
                        $.ajax({
                            type: "POST",
                            url: "http://localhost:8000/save-location/",
                            data: data,
                            success: function (msg) {
                               placeMarkerAndPanTo(e.latLng, map);
                               appendAddressToList(result, addresses_ul);
                            },
                            error: function (errormessage) {
                                var errors_str =  JSON.stringify(errormessage.responseJSON.errors);
                                alert(errors_str);
                            }
                        });
                } else {
                    alert('This is not a street address :(');
                }
            }
        });
    });

    function placeMarkerAndPanTo(latLng, map) {
      var marker = new google.maps.Marker({
        position: latLng,
        map: map
      });
      map.panTo(latLng);
    }

    function appendAddressToList(result, ul) {
        ul.append("<li>" + result.formatted_address + "</li>");
    }
}

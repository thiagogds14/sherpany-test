from django.db import models


class Location(models.Model):
    lat = models.DecimalField('Latitude', max_digits=20, decimal_places=18)
    lng = models.DecimalField('Longitude', max_digits=20, decimal_places=18)
    formatted_address = models.CharField('Address', max_length=255)

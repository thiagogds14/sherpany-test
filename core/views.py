from django.views.generic import View, ListView
from django.http import JsonResponse

import requests

from core.models import Location
from core.forms import LocationForm


def refresh_token():
    url = "https://www.googleapis.com/oauth2/v4/token"
    data = {"client_id":"399576767083-jibolno8slv837v7cf5btuhaagi2e8si.apps.googleusercontent.com", "client_secret":"eCwVNBannpnt00tHGnwku5Eu", "grant_type":"refresh_token","refresh_token":"1/tBg6T9Go-ZlqNl_H-fnxpDEeGPjWTUugq_j9O5aYMr_3YYDHIcwHUJ8t3zdf85GY"}
    response = requests.post(url, data)
    return response.json().get('access_token')


def insert_location_in_ft(lat, lng):
        token = refresh_token()
        headers = {'Authorization': 'Bearer ' + token}
        data = {
            'sql': "INSERT INTO 1VAuRAnKvb2CROkxnkKPonThuZSk2s6fHPZ2XnzRl (Location) VALUES ('{},{}')".format(lat,lng)
        }
        url = 'https://www.googleapis.com/fusiontables/v2/query'
        response = requests.post(url, data, headers=headers)
        return response.status_code, response.json()


class SaveLocationView(View):
    def post(self, request, *args, **kwargs):
        form = LocationForm(request.POST)
        if not form.is_valid():
            return JsonResponse({'errors': form.errors}, status=400)

        if Location.objects.filter(**request.POST.dict()).exists():
            return JsonResponse({})

        status, json_response = insert_location_in_ft(form.cleaned_data['lat'], form.cleaned_data['lng'])
        if status != 200:
            return JsonResponse({'errors': json_response}, status=500)

        form.save()
        return JsonResponse({})


class HomePageView(ListView):
    model = Location
    context_object_name = 'locations'
    template_name = 'core/homepage.html'

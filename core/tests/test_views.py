import json
from unittest import mock
from urllib import parse

from django.test import TestCase
from django.core.urlresolvers import reverse

import responses
from model_mommy.mommy import make

from core.models import Location
from core.views import insert_location_in_ft


class HomePageViewTest(TestCase):
    def setUp(self):
        self.url = reverse('core:homepage')

    def test_correct_response(self):
        response = self.client.get(self.url)

        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response, 'base.html')
        self.assertTemplateUsed(response, 'core/homepage.html')

    def test_load_saved_locations(self):
        make('core.Location', _quantity=3)
        response = self.client.get(self.url)

        self.assertIn('locations', response.context)
        self.assertEqual(3, len(response.context['locations']))


class SaveLocationViewTest(TestCase):
    def setUp(self):
        self.url = reverse('core:save_location')
        self.data = {
            'lat': -22.907391,
            'lng': -43.175665,
            'formatted_address': 'R. da Glória, 108 - Glória, Rio de Janeiro - RJ, 20241-180, Brasil'
        }

    @mock.patch('core.views.insert_location_in_ft')
    def test_save_location_from_post(self, _mock):
        _mock.return_value = (200, {})
        self.assertEqual(0, Location.objects.count())

        response = self.client.post(self.url, self.data)

        self.assertEqual(1, Location.objects.count())

        location = Location.objects.last()
        self.assertEqual(self.data['lat'], float(location.lat))
        self.assertEqual(self.data['lng'], float(location.lng))
        self.assertEqual(self.data['formatted_address'], location.formatted_address)

    @mock.patch('core.views.insert_location_in_ft')
    def test_return_correct_response(self, _mock):
        _mock.return_value = (200, {})
        response = self.client.post(self.url, self.data)

        self.assertEqual(200, response.status_code)
        self.assertEqual('application/json', response['Content-Type'])
        self.assertEqual({}, response.json())

    @mock.patch('core.views.insert_location_in_ft')
    def test_bad_format_request(self, _mock):
        _mock.return_value = (200, {})
        response = self.client.post(self.url, {})

        self.assertEqual(400, response.status_code)
        expected_response = {
            'errors': {
                'formatted_address': ['This field is required.'],
                'lat': ['This field is required.'],
                'lng': ['This field is required.']
            }
        }
        self.assertEqual(expected_response, response.json())

    @mock.patch('core.views.insert_location_in_ft')
    def test_fusion_table_error(self, _mock):
        _mock.return_value = (400, {'error': 'some error'})
        response = self.client.post(self.url, self.data)

        self.assertEqual(500, response.status_code)
        expected_response = {'errors': {'error': 'some error'}}

        self.assertEqual(expected_response, response.json())

    @mock.patch('core.views.insert_location_in_ft')
    def test_dont_save_duplicates(self, _mock):
        _mock.return_value = (200, {})
        self.assertEqual(0, Location.objects.count())
        response = self.client.post(self.url, self.data)

        self.assertEqual(1, Location.objects.count())
        response = self.client.post(self.url, self.data)

        self.assertEqual(1, Location.objects.count())
        response = self.client.post(self.url, self.data)



class FusionTableTest(TestCase):
    @responses.activate
    @mock.patch('core.views.refresh_token')
    def test_correct_params(self, _token):
        _token.return_value = '123'
        responses.add(responses.POST,
                      'https://www.googleapis.com/fusiontables/v2/query',
                      json={}, status=200,
                      content_type='application/json')

        insert_location_in_ft('-22.9060294', '-43.17351450000001')
        call = responses.calls[0]
        request = call.request

        self.assertEqual('Bearer 123', request.headers['Authorization'])
        self.assertEqual('sql=INSERT+INTO+1VAuRAnKvb2CROkxnkKPonThuZSk2s6fHPZ2XnzRl+%28Location%29+VALUES+%28%27-22.9060294%2C-43.17351450000001%27%29', request.body)

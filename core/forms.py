from django.forms import ModelForm

from core.models import Location

# Create the form class.
class LocationForm(ModelForm):
     class Meta:
         model = Location
         fields = ['lat', 'lng', 'formatted_address']

from django.conf.urls import url
from django.views.decorators.csrf import ensure_csrf_cookie

from core.views import SaveLocationView, HomePageView


urlpatterns = [
    url(r'^$',
        ensure_csrf_cookie(HomePageView.as_view()), name='homepage'),
    url(r'save-location/', SaveLocationView.as_view(), name='save_location'),
]


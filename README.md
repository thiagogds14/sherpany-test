SHERPANY Test App

# How to start the aplication
 `virtualenv .ve && source .ve/bin/activate && pip install -r requirements.txt && ./manage.py [migrate|runserver]`

# Project tests
 `./manage.py test`

## Fusion Table Documentation: https://developers.google.com/fusiontables/docs/v2/using

## Fusion Table API Keys:
Because I wanted to keep the Fusion Table alway from the user view I used this site to generate the refreshe Token: https://developers.google.com/oauthplayground/
There I could use my previous generated client and secret keys to generate my user refresh token
